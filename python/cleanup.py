import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
# PIN-Zuweisung LINKS
A=18
B=23
C=24
D=25
time = 0.0008
# PINS definieren
GPIO.setup(A,GPIO.OUT)
GPIO.setup(B,GPIO.OUT)
GPIO.setup(C,GPIO.OUT)
GPIO.setup(D,GPIO.OUT)
GPIO.output(A, False)
GPIO.output(B, False)
GPIO.output(C, False)
GPIO.output(D, False)

# PIN-Zuweisung RECHTS
E=17
F=27
G=22
H=13
time = 0.0008
# PINS definieren
GPIO.setup(E,GPIO.OUT)
GPIO.setup(F,GPIO.OUT)
GPIO.setup(G,GPIO.OUT)
GPIO.setup(H,GPIO.OUT)
GPIO.output(E, False)
GPIO.output(F, False)
GPIO.output(G, False)
GPIO.output(H, False)

GPIO.cleanup()