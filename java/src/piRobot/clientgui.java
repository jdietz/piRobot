package piRobot;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

import javax.swing.*;

public class clientgui {
	JFrame frame = null;
	JButton vor = null;
	JButton zurueck = null;
	JButton links = null;
	JButton rechts = null;
	JTextField steps = null;
	JButton stop = null;
	JTextField time = null;

	public void start() {
		frame = new JFrame("piRobot GUI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new GridLayout(4, 2));
		frame.setResizable(false);
		vor = new JButton("Vor");
		zurueck = new JButton("Zur�ck");
		links = new JButton("Links");
		rechts = new JButton("Rechts");
		steps = new JTextField("100");
		time = new JTextField("800");
		stop = new JButton("STOP!");
		frame.add(vor);
		frame.add(zurueck);
		frame.add(links);
		frame.add(rechts);
		frame.add(steps);
		frame.add(time);
		frame.add(stop);
		frame.pack();
		frame.setVisible(true);
		vor.addActionListener(new vorActionListener());
		zurueck.addActionListener(new zurueckActionListener());
		stop.addActionListener(new stopActionListener());
		links.addActionListener(new linksActionListener());
		rechts.addActionListener(new rechtsActionListener());

	}

	public class vorActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			methods.initVor(Integer.parseInt(steps.getText()), Integer.parseInt(time.getText()));
		}
	}

	public class zurueckActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			methods.initZurueck(Integer.parseInt(steps.getText()), Integer.parseInt(time.getText()));
		}
	}

	public class stopActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			methods.initStop();
		}
	}

	public class linksActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			methods.initLinks(Integer.parseInt(steps.getText()), Integer.parseInt(time.getText()));
		}
	}

	public class rechtsActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			methods.initRechts(Integer.parseInt(steps.getText()), Integer.parseInt(time.getText()));
		}
	}

}