package piRobot;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import gpio.steps;
import network.CConvert;
import network.client_socket;

public class methods {
	static client_socket csocket;

	// Unterscheidungsebene
	public static void initVor(int steps, int time) {
		if (main.cs == 1) {
			sendVor(steps, Integer.toString(time));
		}
		if (main.cs == 3) {
			vor(steps, time);
		}
	}

	public static void initZurueck(int steps, int time) {
		if (main.cs == 1) {
			sendZurueck(steps, Integer.toString(time));
		}
		if (main.cs == 3) {
			zurueck(steps, time);
		}
	}

	public static void initStop() {
		if (main.cs == 1) {
			sendStop();
		}
		if (main.cs == 3) {
			stop();
		}
	}

	public static void initLinks(int steps, int time) {
		if (main.cs == 1) {
			sendLinks(steps, Integer.toString(time));
		} else {
			links(steps, time);
		}
	}

	public static void initRechts(int steps, int time) {
		if (main.cs == 1) {
			sendRechts(steps,Integer.toString(time));
		} else {
			rechts(steps,time);
		}
	}

	// Lokalebene
	public static void links(int steps, int time) {
		gpio.steps.L(false, steps, time);
		gpio.steps.R(true, steps, time);
	}

	public static void rechts(int steps, int time) {
		gpio.steps.R(false, steps, time);
		gpio.steps.L(true, steps, time);
	}

	public static void vor(int steps, int time) {
		gpio.steps.L(false, steps, time);
		gpio.steps.R(false, steps, time);
	}

	public static void zurueck(int steps, int time) {
		gpio.steps.L(true, steps, time);
		gpio.steps.R(true, steps, time);
	}

	public static void stop() {
		steps.stopL = true;
		steps.stopR = true;
	}

	// Sendebene
	public static void sendLinks(int steps, String time) {
		try {
			csocket.send(CConvert.toByte("links; " + steps + ";" + time));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void sendRechts(int steps, String time) {
		try {
			csocket.send(CConvert.toByte("rechts; " + steps + ";" + time));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void sendVor(int steps, String time) {
		try {
			csocket.send(CConvert.toByte("vor;" + steps + ";" + time));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void sendZurueck(int steps, String time) {
		try {
			csocket.send(CConvert.toByte("zurueck;" + steps + ";" + time));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void sendStop() {
		try {
			csocket.send(CConvert.toByte("stop"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
