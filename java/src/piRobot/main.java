package piRobot;

import java.util.concurrent.TimeUnit;

import gpio.steps;
import network.receiveAnalyser;
import network.server_socket;

public class main {
	static int cs = 0; //0=not set, 1=client, 2=server, 3=local
	static int port = 0; //-1 = local
	static String ip = "";
	public static server_socket s;
	public static void main(String[] args) {
		startgui sg = new startgui();
		sg.start();
		while (cs == 0) {
			try {
				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (cs == 1) {
			clientgui cg = new clientgui();
			cg.start();
			System.out.println("Clientmode activated");
		}
		if (cs == 2) {
			steps.init();
			System.out.println("Servermode activated");
			receiveAnalyser.start();
		}
		if (cs == 3) {
			steps.init();
			clientgui cg = new clientgui();
			cg.start();
			System.out.println("Localmode activated");
		}
		
	}	

}