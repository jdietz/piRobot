package piRobot;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import network.client_socket;
import network.server_socket;

public class startgui {
		JFrame frame =null;
		JTextField port = null;
		JTextField ip = null;
		JButton client = null;
		JButton server = null;
		JButton local = null;
		public void start() {
			frame = new JFrame("Startgui");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setLayout(new GridLayout(2,2));
			frame.setResizable(false);
			port = new JTextField("10050");
			ip = new JTextField("192.168.2.5");
			client = new JButton("Clientmode");
			server = new JButton("Servermode");
			local = new JButton("Lokalmode");
			frame.add(client);
			frame.add(server);
			frame.add(ip);
			frame.add(port);
			frame.add(local);
			frame.pack();
			frame.setVisible(true);
			client.addActionListener(new clientActionListener());
			server.addActionListener(new serverActionListener());
			local.addActionListener(new localActionListener());
		}
		public String getIp() {
			return ip.getText();
		}
		public int getPort() {
			return Integer.parseInt(this.port.getText());
		}
public class clientActionListener implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent arg0) {
		main.cs = 1;
		main.port = Integer.parseInt(port.getText());
		main.ip = ip.getText();
		System.out.println(main.port);
		System.out.println(main.ip);
		methods.csocket = new client_socket("SendingUnit",main.ip,main.port);
		methods.csocket.openConnection();
	}
	
}
public class serverActionListener implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent arg0) {
		main.cs = 2;
		main.port = Integer.parseInt(port.getText());
		main.s = new server_socket(main.port);
	}
	
}
public class localActionListener implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent arg0) {
		main.cs = 3;
		main.port = -1;
	}
	
}
}