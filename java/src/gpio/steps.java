package gpio;

import java.util.concurrent.TimeUnit;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiBcmPin;
import com.pi4j.io.gpio.RaspiGpioProvider;
import com.pi4j.io.gpio.RaspiPinNumberingScheme;

public class steps {

	static public Pin B, C, D, E, F, G, H, A = null;
	static GpioPinDigitalOutput AP, BP, CP, DP, EP, FP, GP, HP = null;
	static int timeL, timeR = 800;
	public static boolean stopL, stopR = false;
	final GpioController gpio = GpioFactory.getInstance(); // GPIO INIT
	static private Thread tr, tl = null;

	public static void init() {
		System.out.println(timeL + " | " + timeR);
		// INIT
		// BCM
		GpioFactory.setDefaultProvider(new RaspiGpioProvider(RaspiPinNumberingScheme.BROADCOM_PIN_NUMBERING)); // Numbering
		final GpioController gpio = GpioFactory.getInstance(); // GPIO INIT
		// defining left motor pins
		A = RaspiBcmPin.GPIO_18;
		B = RaspiBcmPin.GPIO_23;
		C = RaspiBcmPin.GPIO_24;
		D = RaspiBcmPin.GPIO_25;

		// defining right motor pins
		E = RaspiBcmPin.GPIO_17;
		F = RaspiBcmPin.GPIO_27;
		G = RaspiBcmPin.GPIO_22;
		H = RaspiBcmPin.GPIO_13;

		// init left pins
		AP = gpio.provisionDigitalOutputPin((Pin) A, "A", PinState.LOW);
		BP = gpio.provisionDigitalOutputPin((Pin) B, "B", PinState.LOW);
		CP = gpio.provisionDigitalOutputPin((Pin) C, "C", PinState.LOW);
		DP = gpio.provisionDigitalOutputPin((Pin) D, "D", PinState.LOW);

		// init right pins
		EP = gpio.provisionDigitalOutputPin((Pin) E, "E", PinState.LOW);
		FP = gpio.provisionDigitalOutputPin((Pin) F, "F", PinState.LOW);
		GP = gpio.provisionDigitalOutputPin((Pin) G, "G", PinState.LOW);
		HP = gpio.provisionDigitalOutputPin((Pin) H, "H", PinState.LOW);

		// set shutdown state left pins
		AP.setShutdownOptions(true, PinState.LOW);
		BP.setShutdownOptions(true, PinState.LOW);
		CP.setShutdownOptions(true, PinState.LOW);
		DP.setShutdownOptions(true, PinState.LOW);

		// set shutdown state right pins
		EP.setShutdownOptions(true, PinState.LOW);
		FP.setShutdownOptions(true, PinState.LOW);
		GP.setShutdownOptions(true, PinState.LOW);
		HP.setShutdownOptions(true, PinState.LOW);
	}
	
	// define rotation
	public static void L(boolean direction, int count, int time) { // direction = true -> backwards
		if (!(tl == null)) {
			if (tl.isAlive()) {
				stopL = true;
				mySleep(10000);

			}
		}
		tl = new Thread() {
			public void run() {
				timeL = time;
				if (direction) {
					for (int i = 0; i < count; i++) {
						if (!stopL) {
							Lbackward();
						} else {
							i = count;
							stopL = false;
						}

					}
				} else {
					for (int i = 0; i < count; i++) {
						if (!stopL) {
							Lforward();
						} else {
							i = count;
							stopL = false;
						}
					}
				}
			}
		};
		tl.start();

	}
	public static void R(boolean direction, int count, int time) { // direction = true -> backwards
		if (!(tr == null)) {
		if (tr.isAlive()) {
			stopR = true;
			mySleep(10000);

		}
		}
		tr = new Thread() {
			public void run() {
				timeR = time;
				if (direction) {
					for (int i = 0; i < count; i++) {
						if (!stopR) {
							Rbackward();
						} else {
							i = count;
							stopR = false;
						}
					}
				} else {
					for (int i = 0; i < count; i++) {
						if (!stopR) {
							Rforward();
						} else {
							i = count;
							stopR = false;
						}
					}
				}
			}
		};
		tr.start();
	}

	// defining 1 steproutine
	private static void Lforward() {
		L1();
		L2();
		L3();
		L4();
		L5();
		L6();
		L7();
		L8();
	}

	private static void Lbackward() {
		L8();
		L7();
		L6();
		L5();
		L4();
		L3();
		L2();
		L1();
	}

	private static void Rforward() {
		R1();
		R2();
		R3();
		R4();
		R5();
		R6();
		R7();
		R8();
	}

	private static void Rbackward() {
		R8();
		R7();
		R6();
		R5();
		R4();
		R3();
		R2();
		R1();
	}

	// defining steps left
	private static void L1() {
		DP.high();
		mySleep(timeL);
		DP.low();
	}

	private static void L2() {
		DP.high();
		CP.high();
		mySleep(timeL);
		DP.low();
		CP.low();
	}

	private static void L3() {
		CP.high();
		mySleep(timeL);
		CP.low();
	}

	private static void L4() {
		BP.high();
		CP.high();
		mySleep(timeL);
		BP.low();
		CP.low();
	}

	private static void L5() {
		BP.high();
		mySleep(timeL);
		BP.low();
	}

	private static void L6() {
		AP.high();
		BP.high();
		mySleep(timeL);
		AP.low();
		BP.low();
	}

	private static void L7() {
		AP.high();
		mySleep(timeL);
		AP.low();
	}

	private static void L8() {
		DP.high();
		AP.high();
		mySleep(timeL);
		DP.low();
		AP.low();
	}

	private static void R1() {
		GP.high();
		mySleep(timeR);
		GP.low();
	}

	private static void R2() {
		GP.high();
		FP.high();
		mySleep(timeR);
		GP.low();
		FP.low();
	}

	private static void R3() {
		FP.high();
		mySleep(timeR);
		FP.low();
	}

	private static void R4() {
		EP.high();
		FP.high();
		mySleep(timeR);
		EP.low();
		FP.low();
	}

	private static void R5() {
		EP.high();
		mySleep(timeR);
		EP.low();
	}

	private static void R6() {
		DP.high();
		EP.high();
		mySleep(timeR);
		DP.low();
		EP.low();
	}

	private static void R7() {
		DP.high();
		mySleep(timeR);
		DP.low();
	}

	private static void R8() {
		GP.high();
		DP.high();
		mySleep(timeR);
		GP.low();
		DP.low();
	}

	private static void mySleep(int time) {
		try {
			TimeUnit.MICROSECONDS.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
