package network;

	import java.io.IOException;
	import java.net.DatagramPacket;
	import java.net.DatagramSocket;
	import java.net.SocketException;
	import java.util.ArrayList;

	public class server_socket implements Runnable {
		private DatagramSocket socket;
		private int port;
		private boolean running = false;
		private Thread run, receive;
		public ArrayList<byte[]> received = new ArrayList<byte[]>();
		String rec_s = null;

		public void stopsocket() {
			socket.close();
		}
		public server_socket(int port) {
			this.port = port;
			try {
				socket = new DatagramSocket(port);
			} catch (SocketException e) {
				e.printStackTrace();
				return;
			}
			run = new Thread(this, "Server");
			run.start();
		}

		public void run() {
			running = true;
			System.out.println("Server started on port " + port);
			receive();

		}

		private void receive() {
			receive = new Thread("Receive") {
				public void start() {
					while (running) {
						byte[] data = new byte[1024];
						DatagramPacket packet = new DatagramPacket(data, data.length);
						try {
							socket.receive(packet);
						} catch (SocketException e) {
						} catch (IOException e) {
							e.printStackTrace();
						}
						received.add(packet.getData());
					}
				}
			};
			receive.start();
		}

	}


