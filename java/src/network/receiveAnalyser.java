package network;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import piRobot.main;
import piRobot.methods;

public class receiveAnalyser {
	public static void start() {
		Thread t = new Thread() {
			public void run() {
				String lastString = "stop";
				int steps = 0;
				String time = "";
				while (true) {
					try {
						if (main.s.received.size() == 0) {
						} else {
							lastString = (String) CConvert.toObject(main.s.received.get(0));
							main.s.received.clear();
						}
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

					if (!(lastString.equals(""))) {
						StringTokenizer st = new StringTokenizer(lastString);
						String direction = st.nextToken(";");
						System.out.println(direction);
						if (!(direction.equals("stop"))) {
							steps = Integer.parseInt(st.nextToken(";"));
							time = st.nextToken(";");
							System.out.println(steps);
							System.out.println(time);
						}
						lastString = "";
						if (direction.equals("vor")) {
							methods.vor(steps, Integer.parseInt(time));
						}
						if (direction.equals("zurueck")) {
							methods.zurueck(steps, Integer.parseInt(time));
						}
						if (direction.equals("stop")) {
							methods.stop();
						}
						if (direction.equals("links")) {
							methods.links(steps, Integer.parseInt(time));
						}
						if(direction.equals("rechts")) {
							methods.rechts(steps, Integer.parseInt(time));
						}
					}
					try {
						TimeUnit.MILLISECONDS.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};
		t.start();
	}
}
